create table part1 (x text);
-- \copy part1 from 'input1' ( format csv, delimiter(','));

-- create lookup table for values found
create table lookup (x text, val integer);
insert into lookup (x, val) values ('0', 0);
insert into lookup (x, val) values ('1', 1);
insert into lookup (x, val) values ('2', 2);
insert into lookup (x, val) values ('3', 3);
insert into lookup (x, val) values ('4', 4);
insert into lookup (x, val) values ('5', 5);
insert into lookup (x, val) values ('6', 6);
insert into lookup (x, val) values ('7', 7);
insert into lookup (x, val) values ('8', 8);
insert into lookup (x, val) values ('9', 9);

-- create function to find where a string is found
create or replace function find_first_occurrence(corpus text, query text)
returns integer as $$
declare
	idx		integer := position(query in corpus);
begin
	if idx <> 0 then
		return idx;
	else
		return 1000000;
	end if;
end;
$$ language plpgsql;

create or replace function find_last_occurrence(corpus text, query text)
returns integer as $$
declare
	idx		integer := position(reverse(query) in reverse(corpus));
begin
	if idx > 0 then
		return idx;
	else
		return 1000000;
	end if;
end;
$$ language plpgsql;

select * from part1 join 

select val, find_last_occurrence('1abc2', lookup.x)
from lookup
order by find_last_occurrence('1abc2', lookup.x) asc
limit 1

-- execute search
select (sum(a)*10+sum(b)) from
(select
	part1.x as x,
	(select val from lookup order by find_first_occurrence(part1.x, lookup.x) asc limit 1) as a,
	(select val from lookup order by find_last_occurrence(part1.x, lookup.x) asc limit 1) as b,
	(select val from lookup order by find_first_occurrence(part1.x, lookup.x) asc limit 1)*10 + (select val from lookup order by find_last_occurrence(part1.x, lookup.x) asc limit 1) as sum
from part1)